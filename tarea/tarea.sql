-- Base de datos utilizada MySql workbench
-- Maria de Lourdes Ponce Ochoa  /  5to semestre / Aplicaciones distribuidas

use tareadba;

-- tabla para la tarea: alumnos

create table alumnos(
		`codigo` int (5) NOT NULL,
        `cedula` varchar(15) DEFAULT NULL,
        `nombres` varchar(40) DEFAULT NULL,
        `apellidos` varchar(40) DEFAULT NULL,
        `direccion` varchar(40) DEFAULT NULL,
        `telefono1` varchar(15) DEFAULT NULL,
        `telefono2` varchar(15) DEFAULT NULL,
        `ciudad` varchar(50) DEFAULT NULL, 
         `pais` varchar(50) DEFAULT NULL,
            `email` varchar(50) DEFAULT NULL,
            `fechanacimiento` date DEFAULT NULL,
            `redsocial` varchar(15) DEFAULT NULL,
            `carrera` varchar(40) DEFAULT NULL,
            `extension` varchar(15) DEFAULT NULL,
            
            `aniograduacion` int (4) DEFAULT NULL,
            `lugardetrabajo` varchar(40) DEFAULT NULL,
            `direcciontrabajo` varchar(40) DEFAULT NULL,
            `telefono` varchar(15) DEFAULT NULL,
            `cargo` varchar(40) DEFAULT NULL,
            `Empresapropia` varchar(5) DEFAULT NULL,
            `trabajaensuareadeestudio` varchar(5) DEFAULT NULL,
            `tiempodetrabajo` int (2) DEFAULT NULL
);

-- select para copiar todos los nombres de las columnas
select * from alumnos;

-- primer insert

insert into alumnos (codigo, cedula, nombres, apellidos, direccion, telefono1, telefono2, ciudad, pais, email, fechanacimiento, redsocial, carrera, extension, aniograduacion, lugardetrabajo, direcciontrabajo, telefono, cargo, Empresapropia, trabajaensuareadeestudio, tiempodetrabajo) values(1,'012','Maria', 'ponce', 'guayaquil', '0939554470', '0939445566', 'guayaquil', 'ecuador', 
'marial@gmail.com', '2000-02-29', 'malu_ochoa', 'software', 'no se', '2021', 'hiper', 'guayaquil', '092892828', 'desarrolladora junior', 'n', 'si', '2'
) ;

-- insert select / respuesta /131077 row(s) affected Records: 131077  Duplicates: 0  Warnings: 0

insert into alumnos (codigo, cedula, nombres, apellidos, direccion, telefono1, telefono2, ciudad, pais, email, fechanacimiento, redsocial, carrera, extension, aniograduacion, lugardetrabajo, direcciontrabajo, telefono, cargo, Empresapropia, trabajaensuareadeestudio, tiempodetrabajo)  select * from alumnos ;

-- aqui creo las tabla basada en la estructura de otra tabla alumnos sin copiar los datos.
CREATE TABLE alumnos_aniograduacion_2022 AS SELECT * FROM alumnos WHERE 1 = 0;
CREATE TABLE alumnos_aniograduacion_2024 AS SELECT * FROM alumnos WHERE 1 = 0;

-- consultas de verificacion 

select * from alumnos_aniograduacion_2022;
select * from alumnos_aniograduacion_2024;

-- Ejercicio de fragmentacion horizontal

INSERT INTO alumnos_aniograduacion_2022
SELECT * FROM alumnos
WHERE aniograduacion = 2022;

INSERT INTO alumnos_aniograduacion_2024
SELECT * FROM alumnos
WHERE aniograduacion = 2024;





